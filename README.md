# twitter_automation

ツイッターの自動化に関するリポジトリ。

基本システムのpython3で行う。（mecabの都合）

## config.py

twitter developersから得た以下のキー情報を記載する。

```
CONSUMER_KEY='your consumer key'
CONSUMER_SECRET='your consumer secret key'
ACCESS_TOKEN='your access token'
ACCESS_TOKEN_SECRET='your access token secret key'
```
## 環境設定

.pyenvでpython=3.8.11をインストールした環境と想定している。

### python標準機能で仮想環境作成

`python -m venv venv`

### 仮想環境に入る

`source venv/bin/activate`

### ライブラリのインストール

`pip install -r requirements.txt`

