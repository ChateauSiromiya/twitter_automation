import MeCab
import matplotlib.pyplot as plt
import csv
from wordcloud import WordCloud
from collections import Counter
import nlplot
import tweepy
import config
import datetime
import pandas as pd
import plotly.io as pio
import kaleido
import random
import os
import shutil

 # TwitterAPIの認証データを取得して認証
CK = config.CONSUMER_KEY
CS = config.CONSUMER_SECRET
AT = config.ACCESS_TOKEN
ATS = config.ACCESS_TOKEN_SECRET
PATH = config.BASE_PATH

auth = tweepy.OAuth1UserHandler(CK, CS, AT, ATS)
api = tweepy.API(auth, wait_on_rate_limit=True)

def initiarize():
    shutil.rmtree(f"{PATH}/twitter_automation/tmp")
    os.makedirs(f"{PATH}/twitter_automation/tmp",exist_ok=True)

def get_timeline(count=100):
    #つぶやきを格納するリスト
    tweetsList = []
    tweets = api.home_timeline(count=count)
    for tweet in tweets:
        #ツイートテキストをリストに追加
        tweetsList.append(tweet.text)
    return tweetsList

def mecab_text(tweet):
    #Mecabを使用して、形態素解析
    mecab = MeCab.Tagger("-Ochasen")

    #"名詞", "動詞", "形容詞", "副詞"を格納するリスト
    word_list = []
    #Mecabで形態素解析を実施
    node = mecab.parseToNode(tweet)
    while node:
        word_type = node.feature.split(",")[0]
        word_type_about = node.feature.split(",")[1]
        yomi      = node.feature.split(",")[-1]
        #取得する単語は、"名詞", "動詞", "形容詞", "副詞"
        if word_type in ["名詞", "動詞", "形容詞", "副詞"] \
            and len(node.surface)>2 \
            and yomi != "*" \
            and word_type_about not in ["接尾","非自立","代名詞","副詞可能"]:
            word_list.append(node.surface)
        node = node.next
    return word_list

def analyze_tweet(tweet_list):
    df = pd.DataFrame(tweet_list, columns=['text'])
    
    #形態素結果をリスト化し、データフレームdf1に結果を列追加する
    df['words'] = df['text'].apply(mecab_text)

    npt = nlplot.NLPlot(df, target_col='words')

    # top_nで頻出上位単語, min_freqで頻出下位単語を指定できる
    # ストップワーズは設定しませんでした。。。
    # stopwords = npt.get_stopword(top_n=0, min_freq=0)
    
    # ストップワードの設定　※これは検索キーワードによって除外したほうがいい単語を設定
    stop_words = [ 'https','RT','t','@','今日','エイプリル','エイプリルフール','co','the','of','Summit','Tokyo','Japan',u'説明',u'データ',u'する',u'オラクル',u'日本',u'提供',u'開催',u'お客様','ちんぽ','ちんちん','ちんこ','まんこ','フォロー']
    
    fig_unigram = npt.bar_ngram(
        title='uni-gram',
        xaxis_label='word_count',
        yaxis_label='word',
        ngram=1,
        top_n=20,
        width=800,
        height=1100,
        color=None,
        horizon=True,
        stopwords=stop_words,
        verbose=False,
        save=False,
    )
    fig_unigram.write_image(f"{PATH}/twitter_automation/tmp/unigram.png")
    
    # ビルド（データ件数によっては処理に時間を要します）※ノードの数のみ変更
    #npt.build_graph(stopwords=stop_words, min_edge_frequency=1)

    #fig_network = npt.co_network(
    #     title='Co-occurrence network',
    #     sizing=100,
    #     node_size='adjacency_frequency',
    #     color_palette='hls',
    #     width=1100,
    #     height=700,
    #     save=False
    # )
    #pio.write_image(fig_network, f"{PATH}/tmp/network.png", engine="kaleido")
    
    words_list = []
    for word_list in df['words'].to_list():
        for word in word_list:
            words_list.append(word)
    
    #出現回数を集計し、最頻順にソート
    words_count = Counter(words_list)
    result = words_count.most_common()
    
    new_result = []
    for word, cnt in result:
        if cnt > 2:
            new_result.append((word, cnt))
        else:
            break
    
    result_dic = dict(new_result)
    
    font_path = f"{PATH}/twitter_automation/fonts/SourceHanSerifK-Light.otf"
    #解析した単語、ストップワードを設定、背景の色は黒にしてます
    wordcloud = WordCloud(background_color="black",
                          font_path=font_path,
                          stopwords=set(stop_words),
                          colormap="prism",
                          width=1200,
                          height=800).fit_words(result_dic)

    wordcloud.to_file(f"{PATH}/twitter_automation/tmp/wordcloud.png")

def post_tweet():
    ri = random.randint(1,10000)
    tweet_txt = f"第{ri}話"
    
    img1 = f"{PATH}/twitter_automation/tmp/wordcloud.png"
    img2 = f"{PATH}/twitter_automation/tmp/unigram.png"
    #img3 = f"{PATH}/twitter_automation/tmp/network.png"
    media1 = api.media_upload(img1)
    media2 = api.media_upload(img2)
    #media3 = api.media_upload(img3)
    
    api.update_status(status=tweet_txt, media_ids=[media1.media_id, media2.media_id])

if __name__ == '__main__':
    initiarize()
    tweetsList = get_timeline(200)
    analyze_tweet(tweetsList)
    post_tweet()
