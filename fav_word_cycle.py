import config
import tweepy
import time

# TwitterAPIの認証データを取得して認証
CK = config.CONSUMER_KEY
CS = config.CONSUMER_SECRET
AT = config.ACCESS_TOKEN
ATS = config.ACCESS_TOKEN_SECRET

auth = tweepy.OAuth1UserHandler(CK, CS, AT, ATS)
api = tweepy.API(auth,wait_on_rate_limit=True)

fav_count = 0
loop_out = False
for loop_count in range(7):
    print("----------------------------------")
    print(str(loop_count + 1) + "回目のループ開始！")
    print("----------------------------------")
    if loop_count == 0:
        query = ["ホームズ","好き"]
    elif loop_count == 1:
        query = ["探偵","好き"]
    elif loop_count == 2:
        query = ["AI","好き"]
    elif loop_count == 3:
        query = ["機械学習","好き"]
    elif loop_count == 4:
        query = ["FGO","好き"]
    elif loop_count == 5:
        query = ["サイバーパンク","好き"]
    elif loop_count == 6:
        query = ["SF","好き"]
    elif loop_count == 7:
        query = "読み専"
    # Max100人までしか検索できない。単語検索結果で出てきたアカウント数が上限となる。
    search_count = 70
    results = api.search_tweets(q=query, count=search_count)
    for result in results:
        #  いいねした人数が特定人数以上になったらループ抜けて処理終了。
        if fav_count > 400:
            loop_out = True
            break
        user_id = result.user.id
        user_name = result.user.name
        tweet = result.text
        tweet_id = result.id

        print("ユーザー名：" + user_name)
        print("ユーザーID：" + str(user_id))
        print("-----------------------------")

        try:
            api.create_favorite(tweet_id)  # ファボする
            print(tweet)
            print("-----------------------------")
            print("をファボしました( ੭˙꒳ ˙)੭\n\n")
            print("-----------------------------")
            fav_count += 1
            time.sleep(3)
        except:
            print(tweet)
            print("-----------------------------")
            print("はファボしてます('ω')\n\n")
            print("-----------------------------")

    print("----------------------------------")
    print(str(loop_count + 1) + "回目のループが終了しました")
    print("----------------------------------")
    # ファボ上限になったらループ抜ける
    if loop_out:
        break
    # アクセス連続しすぎるとやばいかもだから5分待つ（5分待つことで、153APIアクセス/5分 = 459APIアクセス/15分でAPIアクセス上限）
    print("5分待ちます")
    time.sleep(300)
